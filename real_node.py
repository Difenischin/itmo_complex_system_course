# To change this template, choose Tools | Templates
# and open the template in the editor.

__author__="marcoj"
__date__ ="$17-ott-2013 23.51.29$"
import random

class real_node:


    def __init__(self, name):
        self.name = name
        self.spin = 0
        self.neigh = []
        self.neigh_temp = []
        self.neigh_temp_w = []
        self.behavior = random.random()
        
        self.degree = 0
    
    def displayNode(self):
        print("Node %d " % self.name)

    def displayNodeNeigh(self):
        print("Node %d " % self.name)
        print("List Neigh: ",self.neigh)

    def AddNeigh(self,new):
        self.neigh.append(new)
    
    def setDegree(self):
        self.degree = len(self.neigh)
        
    def RemoveNeigh(self,new):
        self.neigh.remove(new)

    def AddNeighTemp(self,new):
        self.neigh_temp.append(new)

    def RemoveNeighTemp(self,new):
        self.neigh_temp.remove(new)

    def AddNeighTempW(self,new):
        self.neigh_temp_w.append(new)
    
    def RemoveNeighTempW(self,new):
        self.neigh_temp_w.remove(new)
        
    def UpdateSpin(self,spin):
        self.spin = spin
